
# import pre tested plans from the package
from bluesky.plan_stubs import mov
import bluesky.plan_stubs as bps
from bluesky.plans import scan, tune_centroid
from bluesky.preprocessors import relative_set_wrapper


def dcm_energy_creep(energy, num_attempts = 10,c2t_prc = 3.0):

    """
    This plan will move the DCM from it's current energy, to a new energy

    It will attempt this repeatedly until the c2t setpoint - readback is less than the c2t_prc

    It will attempt it num_attempts times

    arguments:

        energy: float
            The required energy to move to
        num_attempts : int
            The maximum number of times we will try to move the DCM energy and achieve required c2t
        c2t_prc : float
            The precision in um of the c2t setting when we move the energy
      
    
    """

    c2t_diff = c2t_prc+1.0 # initialise to allow loop to run initially
    attempts = 0

    # Repeatedly attempt to move the dcm energy and monitor the c2t difference
    while c2t_diff > c2t_prc and attempts < num_attempts:

        # move the energy to -100 below the desired energy, wait for it to get there
        print(f"Moving DCM Energy to {energy-100} ")
        yield from mov(u17_dcm.en,energy-100)

        # move the energy to -10 below the desired energy, wait for it to get there
        print(f"Moving DCM Energy to {energy-10} ")
        yield from mov(u17_dcm.en,energy-10)

        # move the energy to the desired energy, wait for it to get there
        print(f"Moving DCM Energy to {energy} ")
        yield from mov(u17_dcm.en,energy)

        c2t_diff = abs(u17_dcm.c2t.setpoint.get() -u17_dcm.c2t.readback.get())
        print(f"The difference between the ct2 setpoint and readback is {c2t_diff} ")
        attempts = attempts + 1

    if attempts >= num_attempts:

        msg = (
            f"Unable to make the c2t_diff less than {c2t_prc} in {num_attempts} attempts"
        )
        raise ValueError(msg)


def dcm_change_energy(energy, num_attempts = 10,c2t_prc = 3.0, gap_prec = 1, fbp_prec = 0.1):

    """
    This plan will move the DCM from it's current energy, to a new energy

    It will then find the optimal ID gap and DCM 2nd Crystal Pitch for max flux by measuring the current on kth9

    arguments:

        energy: float
            The required energy to move to
        num_attempts : int
            The maximum number of times we will try to move the DCM energy and achieve required c2t
        c2t_prc : float
            The precision in um of the c2t setting when we move the energy
        gap_prec: float (um)
            default(1)
            The required precision of the gap optimistation
        fbp_prec: float (urad)
            default(0.1)
            The required precision of the DCM 2nd Crystal Pitch optimisation
    
    """

    #ID off. mono should not control the undulator
    yield from IDoff()

    #disable 2nd crystal translation (ccoff)
    yield from configure(u17_dcm,{"channelcut_disable":"1"})

    # Move the dcm energy until the difference between the c2t setpoint and readback is less than c2t_prc, try num_attempts times
    yield from dcm_energy_creep(energy, num_attempts, c2t_prc)

    #enable 2nd crystal translation (ccoff)
    yield from configure(u17_dcm,{"channelcut_disable":"0"})

    #ID on. mono should control the undulator
    yield from IDon()

    #1 move in the diode, wait for it to finish
    yield from mov(diode_motor,100)

    #determine gap_0
    gap_0 = u17.gap.position

    #scan the undulator gap, and leave it at the max flux
    yield from tune_centroid([kth9],'kth9',u17.gap, gap_0-0.03, gap_0+0.03,gap_prec/1000,10, md={"reason":"finding gap value that gives dcm max flux"})

    #determine fbp_0
    fbp_0 = u17_dcm.cr1.position

    #scan the crystal pitch, and leave it at the max flux
    yield from tune_centroid([kth9],'kth9',u17_dcm.cr1, fbp_0-20,fbp_0+20,fbp_prec,10,md={"reason":"finding the dcm 2nd Crystal pitch that gives dcm max flux"})

    #When complete move the diode out
    yield from mov(diode_motor,0)

    #print the result
    print(f"The id_gap has been set to {u17.gap.position} the crystal has been moved to {u17_dcm.cr1.position} ")
